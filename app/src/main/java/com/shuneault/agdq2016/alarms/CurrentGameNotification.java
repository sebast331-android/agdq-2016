package com.shuneault.agdq2016.alarms;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.shuneault.agdq2016.AppSingleton;
import com.shuneault.agdq2016.MainActivity;
import com.shuneault.agdq2016.R;
import com.shuneault.agdq2016.intents.ExternalIntents;
import com.shuneault.agdq2016.objects.GameSchedule;
import com.shuneault.agdq2016.settings.SettingsFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by sebast on 03/01/16.
 */
public class CurrentGameNotification extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        updateNotification(context, intent);
    }

    public void updateNotification(Context context, Intent intent) {
        AppSingleton mySingleton = AppSingleton.getInstance(context);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Boolean bShowNotif = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SettingsFragment.PREF_ONGOING_NOTIFICATION, true);
        DateFormat df = SimpleDateFormat.getTimeInstance(DateFormat.SHORT);
        String strNotifText = "";
        int nextGameCount = 0;
        Log.i("LOGCAT", "OngoingNotification: " + bShowNotif);

        // Set the notification
        if (bShowNotif) {
            // Find the running game, start time and end time
            GameSchedule currentGame = null;
            for (GameSchedule nextGame : mySingleton.getGameList()) {
                if (System.currentTimeMillis() > nextGame.getDate().getTimeInMillis()) {
                    currentGame = nextGame;
                } else {
                    // Found the current game, get the next 3
                    if (nextGameCount >= 3) {
                        break;
                    } else {
                        strNotifText = strNotifText + (strNotifText.isEmpty()?"":"\n") + df.format(nextGame.getDate().getTime()) + ": " + nextGame.getName();
                        nextGameCount++;
                    }
                }
            }

            // Update the notification
            if (currentGame != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel notificationChannel = new NotificationChannel("M_CH_ID", "My Notifications", NotificationManager.IMPORTANCE_LOW);

                    // Configure the notification channel.
                    notificationChannel.setDescription("Channel description");
                    notificationChannel.enableLights(false);
                    notificationManager.createNotificationChannel(notificationChannel);
                }

                Intent mainActivityIntent = new Intent(context, MainActivity.class);
                mainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                PendingIntent clickPending = PendingIntent.getActivity(context, 0, mainActivityIntent, 0);
                PendingIntent twitchPending = PendingIntent.getActivity(context, 0, new ExternalIntents(context).getTwitchIntent(), PendingIntent.FLAG_UPDATE_CURRENT);
                PendingIntent donatePending = PendingIntent.getActivity(context, 0, new ExternalIntents(context).getDonationIntent(), PendingIntent.FLAG_UPDATE_CURRENT);
                NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "M_CH_ID");
                builder.setOngoing(true);
                builder.setContentIntent(clickPending);
                builder.addAction(R.drawable.ic_twitch, context.getString(R.string.watch_on_twitch), twitchPending);
                builder.addAction(R.drawable.ic_donate, context.getString(R.string.donate), donatePending);
                builder.setSmallIcon(R.drawable.ic_notification);
                builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
                builder.setContentTitle(currentGame.getName());
                builder.setContentText(context.getString(R.string.playing_since) + df.format(currentGame.getDate().getTime()));
                builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
                builder.setStyle(new NotificationCompat.BigTextStyle().bigText(strNotifText));
                notificationManager.notify(AppSingleton.NOTIF_CURRENT_GAME, builder.build());
            }
        } else {
            // Cancel the notification
            notificationManager.cancel(AppSingleton.NOTIF_CURRENT_GAME);
        }
    }
}
