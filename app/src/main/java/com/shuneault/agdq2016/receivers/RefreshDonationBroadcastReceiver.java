package com.shuneault.agdq2016.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.shuneault.agdq2016.AsyncTaskGetDonation;

/**
 * Created by sebast on 02/01/16.
 */
public class RefreshDonationBroadcastReceiver extends BroadcastReceiver {

    public static final String BROADCAST_REFRESH_DONATION = "com.shuneault.agdq2016.BROADCAST_REFRESH_DONATION";
    public static final String EXTRA_DONATION_AMOUNT = "com.shuneault.agdq2016.EXTRA_DONATION_AMOUNT";

    @Override
    public void onReceive(final Context context, Intent intent) {
        Log.i("LOGCAT", "RefreshDonationBroadcastReceiver onReceive");
        (new AsyncTaskGetDonation(new AsyncTaskGetDonation.OnThreadProgress() {
            @Override
            public void onFinish(String donation) {
                Intent brIntent = new Intent(BROADCAST_REFRESH_DONATION);
                brIntent.putExtra(EXTRA_DONATION_AMOUNT, donation);
                context.sendBroadcast(brIntent);
                Log.i("LOGCAT", "RefreshDonationBroadcastReceiver onFinish");
            }
        })).execute();
    }
}
