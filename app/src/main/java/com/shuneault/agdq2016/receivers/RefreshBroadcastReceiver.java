package com.shuneault.agdq2016.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.shuneault.agdq2016.AppSingleton;
import com.shuneault.agdq2016.WebCodeSource;
import com.shuneault.agdq2016.alarms.ScheduledService;
import com.shuneault.agdq2016.objects.GameSchedule;

import java.util.ArrayList;

/**
 * Created by sebast on 02/01/16.
 */
public class RefreshBroadcastReceiver extends BroadcastReceiver {

    public static final String BROADCAST_REFRESH_GAMELIST = "com.shuneault.agdq2016.BROADCAST_REFRESH_GAMELIST";

    private AppSingleton mySingleton;

    @Override
    public void onReceive(final Context context, Intent intent) {
        mySingleton = AppSingleton.getInstance(context);
        (new WebCodeSource(new WebCodeSource.OnThreadProgress() {
            @Override
            public void onProgress(Integer progress) {

            }

            @Override
            public void onFinish(ArrayList<GameSchedule> gameSchedule) {
                mySingleton.clearGames();
                mySingleton.addAllGames(gameSchedule);
                Intent brIntent = new Intent(BROADCAST_REFRESH_GAMELIST);
                context.sendBroadcast(brIntent);

                // Refresh all alarms
                for (GameSchedule item : gameSchedule) {
                    if (mySingleton.getReminders().containsKey(item.getName())) {
                        if (System.currentTimeMillis() > item.getDate().getTimeInMillis()) {
                            // Do not refresh the alarm
                        } else {
                            // Refresh the alarm
                            Intent intent = new Intent(context, ScheduledService.class);
                            intent.putExtra("Game", item.getName());
                            intent.putExtra("GameId", item.getId());
                            PendingIntent sender = PendingIntent.getService(context, item.getId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
                            AlarmManager mgr = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                            mgr.set(AlarmManager.RTC_WAKEUP, item.getDate().getTimeInMillis(), sender);
                        }
                    }
                }
            }
        })).execute();
    }
}
