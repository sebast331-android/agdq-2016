package com.shuneault.agdq2016;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shuneault.agdq2016.alarms.CurrentGameNotification;
import com.shuneault.agdq2016.receivers.RefreshBroadcastReceiver;
import com.shuneault.agdq2016.receivers.RefreshDonationBroadcastReceiver;
import com.shuneault.agdq2016.objects.GameSchedule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by sebast on 31/12/15.
 */
public class AppSingleton {
    private static AppSingleton ourInstance = new AppSingleton();
    private Context mContext = null;
    private HashMap<String, GameSchedule> mReminders = new HashMap<>();
    private ArrayList<GameSchedule> mGameList = new ArrayList<>();

    public static final String SHARED_PREF_AGDQ = "SHARED_PREF_AGDQ";
    public static final String PREF_REMINDERS = "PREF_REMINDERS";
    public static final String PREF_SCHEDULE = "PREF_SCHEDULE";
    public static final String PREF_CURRENT_GAME_NOTIFICATION = "PREF_CURRENT_GAME_NOTIFICATION";

    public static final int NOTIF_CURRENT_GAME = 1001;

    public static AppSingleton getInstance(Context context) {
        if (ourInstance.mContext != null) return ourInstance;

        ourInstance.mContext = context;
        ourInstance.loadGameList();
        ourInstance.loadReminders();

        // Automatic Refresh of Game List
        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, RefreshBroadcastReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mgr.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 60 * 5, sender); // Update every 5 minutes

        // Automatic Refresh of Donations
        Intent intentDonation = new Intent(context, RefreshDonationBroadcastReceiver.class);
        PendingIntent senderDonation = PendingIntent.getBroadcast(context, 0, intentDonation, PendingIntent.FLAG_UPDATE_CURRENT);
        mgr.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), 1000 * 60 * 5, senderDonation); // Update every 5 minutes

        // Ongoing notification for current game
        Intent intentOngoingGame = new Intent(context, CurrentGameNotification.class);
        PendingIntent senderOngoingGame = PendingIntent.getBroadcast(context, NOTIF_CURRENT_GAME, intentOngoingGame, PendingIntent.FLAG_UPDATE_CURRENT);
        mgr.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), 1000 * 60 * 5, senderOngoingGame);
        new CurrentGameNotification().updateNotification(context, intentOngoingGame);
        return ourInstance;
    }

    private AppSingleton() {
    }


    public boolean addReminder(GameSchedule gameSchedule) {
        mReminders.put(gameSchedule.getName(), gameSchedule);
        return saveReminders();
    }

    public boolean removeReminder(String gameName) {
        mReminders.remove(gameName);
        return saveReminders();
    }

    public void clearReminders() {
        mReminders.clear();
        saveReminders();
    }

    public ArrayList<GameSchedule> getGameList() {
        return mGameList;
    }

    public void clearGames() {
        mGameList.clear();
        saveGameList();
    }

    public void addGame(GameSchedule gameSchedule) {
        mGameList.add(gameSchedule);
        saveGameList();
    }

    public void addAllGames(ArrayList<GameSchedule> gameSchedules) {
        mGameList.addAll(gameSchedules);
        saveGameList();
    }


    public HashMap<String, GameSchedule> getReminders() {
        return mReminders;
    }

    public boolean saveGameList() {
        if (mContext == null) return false;
        Gson gson = new Gson();
        mContext.getSharedPreferences(SHARED_PREF_AGDQ, Context.MODE_PRIVATE)
                .edit()
                .putString(PREF_SCHEDULE, gson.toJson(mGameList))
                .apply();
        return true;
    }

    public boolean loadGameList() {
        if (mContext == null) return false;
        Gson gson = new Gson();
        mGameList = gson.fromJson(mContext.getSharedPreferences(SHARED_PREF_AGDQ, Context.MODE_PRIVATE)
                .getString(PREF_SCHEDULE, ""), new TypeToken<ArrayList<GameSchedule>>(){}.getType());
        if (mGameList == null) mGameList = new ArrayList<>();
        return true;
    }

    public boolean saveReminders() {
        if (mContext == null) return false;
        Gson gson = new Gson();
        mContext.getSharedPreferences(SHARED_PREF_AGDQ, Context.MODE_PRIVATE)
                .edit()
                .putString(PREF_REMINDERS, gson.toJson(mReminders))
                .commit();
        return true;
    }

    public boolean loadReminders() {
        if (mContext == null) return false;
        Gson gson = new Gson();
        mReminders = gson.fromJson(mContext.getSharedPreferences(SHARED_PREF_AGDQ, Context.MODE_PRIVATE)
                .getString(PREF_REMINDERS, ""), new TypeToken<HashMap<String, GameSchedule>>(){}.getType());
        if (mReminders == null) mReminders = new HashMap<>();
        // Remove old reminders
        Iterator<GameSchedule> iter = mReminders.values().iterator();
        while (iter.hasNext()) {
            GameSchedule iterNext = iter.next();
            if (System.currentTimeMillis() > iterNext.getDate().getTimeInMillis()) {
                iter.remove();
            }
        }
        return true;
    }


}
