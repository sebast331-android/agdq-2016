package com.shuneault.agdq2016.objects;

import java.util.Calendar;

/**
 * Created by sebast on 31/12/15.
 */
public class GameSchedule {
    private String mName;
    private Calendar mDate;
    private String mRunners;
    private String mRunTime;
    private String mCategory;
    private String mSetupTime;
    private String mDescription;

    public GameSchedule(String name, Calendar date, String runners, String runTime, String category, String setupTime, String description) {
        mName = name;
        mDate = date;
        mRunners = runners;
        mRunTime = runTime;
        mCategory = category;
        mSetupTime = setupTime;
        mDescription = description;
    }

    public int getId() {
        return mName.hashCode();
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getName() {


        return mName;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getSetupTime() {
        return mSetupTime;
    }

    public String getCategory() {
        return mCategory;
    }

    public String getRunTime() {
        return mRunTime;
    }

    public Calendar getDate() {
        return mDate;
    }

    public void setDate(Calendar mDate) {
        this.mDate = mDate;
    }

    public String getRunners() {
        return mRunners;
    }
}
